package ordination;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ArrayList;

public class DagligSkaev extends Ordination
{
	private ArrayList<Dosis> doser = new ArrayList<>();

	public DagligSkaev(LocalDate startDen, LocalDate slutDen, Laegemiddel laegemiddel, Patient patient)
	{
		super(startDen, slutDen, laegemiddel, patient);
	}

	public ArrayList<Dosis> getDoser()
	{
		return new ArrayList<>(doser);
	}

	public void opretDosis(LocalTime tid, double antal)
	{
		Dosis dosis = new Dosis(tid, antal);
		doser.add(dosis);
		// ingen return statement?
	}

	// overhovedet nødvendig?
	public void deleteDosis(Dosis dosis)
	{
		doser.remove(dosis);
	}

	/**
	 * returnerer den samlede dosis
	 *
	 * @return den samlede dosis
	 */
	@Override
	public double samletDosis()
	{
		double result = 0;
		for (Dosis d : doser)
		{
			result = result + d.getAntal();
		}
		return result;
	}

	/**
	 * returnerer døgnprisen
	 *
	 * @return den udregnede gennemsnitlige dosis
	 */
	@Override
	public double doegnDosis()
	{
		double dosageTotal = 0;
		for (Dosis d : doser)
		{
			dosageTotal = +d.getAntal();
		}
		return dosageTotal / antalDage();
	}

	/**
	 * Returnerer typen af ordination
	 *
	 * @return typen af ordination
	 */
	@Override
	public String getType()
	{
		return "Skæv";
	}
}

package ordination;

import java.time.LocalDate;
import java.time.LocalTime;
import java.time.temporal.ChronoUnit;

public class DagligFast extends Ordination
{
	private Dosis[] doser = new Dosis[4];

	public DagligFast(LocalDate startDen, LocalDate slutDen, Laegemiddel laegemiddel, Patient patient, Dosis[] doser)
	{
		super(startDen, slutDen, laegemiddel, patient);
		this.doser = doser;
		// Alternativt, kan vi vedligeholde kodestil og have doser i
		// constructor, som ArrayList og bruge opretDosis() til at teste om
		// arrayet opfylder krav.
	}

	public Dosis[] getDoser()
	{
		return new Dosis[4];
	}

	public void opretDosis(LocalTime tid, double antal)
	{
		Dosis dosis = new Dosis(tid, antal);

		if (tid.isAfter(LocalTime.of(5, 59)) && tid.isBefore(LocalTime.of(12, 00)))
		{
			doser[0] = dosis;
		} else if (tid.isAfter(LocalTime.of(11, 59)) && tid.isBefore(LocalTime.of(18, 00)))
		{
			doser[1] = dosis;
		} else if (tid.isAfter(LocalTime.of(17, 59)) && tid.isBefore(LocalTime.of(00, 00)))
		{
			doser[2] = dosis;
		} else if (tid.isAfter(LocalTime.of(23, 59)) && tid.isBefore(LocalTime.of(6, 00)))
		{
			doser[3] = dosis;
		} else
		{
			System.out.println("fejl!");
		}
	}

	@Override
	public double samletDosis()
	{
		double multiplier = ChronoUnit.DAYS.between(getStartDen(), getSlutDen());
		return doegnDosis() * multiplier;
	}

	@Override
	public double doegnDosis()
	{
		double dailyTotal = 0;
		for (int i = 0; i < doser.length; i++)
		{
			dailyTotal = +doser[i].getAntal();
		}
		return dailyTotal;
	}

	@Override
	public String getType()
	{
		return "Fast";
	}
}
package ordination;

import java.time.LocalDate;
import java.time.temporal.ChronoUnit;

public abstract class Ordination
{
	private LocalDate startDen;
	private LocalDate slutDen;
	private Laegemiddel laegemiddel;

	/**
	 * En ordination består af et lægemiddel, en startdato og slutdato.
	 * 
	 * @param startDen
	 * @param slutDen
	 * @param laegemiddel
	 */

	public Ordination(LocalDate startDen, LocalDate slutDen, Laegemiddel laegemiddel, Patient patient)
	{
		this.startDen = startDen;
		this.slutDen = slutDen;
		this.laegemiddel = laegemiddel;
		// usikker på kode
		patient.addOrdination(this);
	}

	/**
	 * Returner en ordination' lægemiddel
	 * 
	 * @return ordination' lægemiddel
	 */

	public Laegemiddel getLaegemiddel()
	{
		return laegemiddel;
	}

	/**
	 * Vælg lægemiddel, til ordination
	 */
	public void setLaegemiddel()
	{
		this.laegemiddel = laegemiddel;
	}

	/**
	 * Dato for en ordination' start
	 * 
	 * @return dato, i LocalDate
	 */
	public LocalDate getStartDen()
	{
		return startDen;
	}

	/**
	 * Dato for en ordination' ende.
	 * 
	 * @return dato, i LocalDate
	 */
	public LocalDate getSlutDen()
	{
		return slutDen;
	}

	/**
	 * Antal hele dage mellem startdato og slutdato. Begge dage inklusive.
	 * 
	 * @return antal dage ordinationen gælder for
	 */
	public int antalDage()
	{
		return (int) ChronoUnit.DAYS.between(startDen, slutDen) + 1;
	}

	@Override
	public String toString()
	{
		return startDen.toString();
	}

	/**
	 * Returnerer den totale dosis der er givet i den periode ordinationen er
	 * gyldig
	 * 
	 * @return
	 */
	public abstract double samletDosis();;

	/**
	 * Returnerer den gennemsnitlige dosis givet pr dag i den periode
	 * ordinationen er gyldig
	 * 
	 * @return
	 */
	public abstract double doegnDosis();

	/**
	 * Returnerer ordinationstypen som en String
	 * 
	 * @return
	 */
	public abstract String getType();
}

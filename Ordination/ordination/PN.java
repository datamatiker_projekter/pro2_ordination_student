package ordination;

import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;

public class PN extends Ordination
{

	private double antalEnheder;
	private ArrayList<LocalDate> datoListe = new ArrayList<>();

	/**
	 * En ordination består af et lægemiddel, en startdato og slutdato.
	 *
	 * @param startDen
	 * @param slutDen
	 * @param laegemiddel
	 */
	public PN(LocalDate startDen, LocalDate slutDen, Laegemiddel laegemiddel, Patient patient, double antalEnheder)
	{
		super(startDen, slutDen, laegemiddel, patient);
		this.antalEnheder = antalEnheder;
	}

	/**
	 * Registrerer at der er givet en dosis paa dagen givesDen Returnerer true
	 * hvis givesDen er inden for ordinationens gyldighedsperiode og datoen
	 * huskes Retrurner false ellers og datoen givesDen ignoreres
	 *
	 * @param givesDen
	 * @return
	 */
	public boolean givDosis(LocalDate givesDen)
	{
		boolean dosisGivet = false;
		// hvis datoen er mellem start og slut datoen, gives dosissen og datoen
		// bliver added til listen
		if (givesDen.isAfter(getStartDen()) && givesDen.isBefore(getSlutDen()))
		{
			datoListe.add(givesDen);
			dosisGivet = true;
		}
		return dosisGivet;
	}

	@Override
	public double doegnDosis()
	{
		double døgndosis = 0;

		if (getSlutDen().isAfter(getStartDen()) && antalEnheder >= 0
				|| getSlutDen() == getStartDen() && antalEnheder >= 0)
		{
			double divider = ChronoUnit.DAYS.between(getStartDen(), getSlutDen());
			double totalEnheder = antalEnheder * datoListe.size();
			døgndosis = totalEnheder / divider;
		} else
		{
			throw new IllegalArgumentException("");

		}

		return døgndosis;
	}

	@Override
	public String getType()
	{
		return "PN";
	}

	@Override
	public double samletDosis()
	{
		return doegnDosis() * getAntalGangeGivet();
	}

	/**
	 * Returnerer antal gange ordinationen er anvendt
	 *
	 * @return
	 */
	public int getAntalGangeGivet()
	{
		return datoListe.size();
	}

	public double getAntalEnheder()
	{
		return antalEnheder;
	}

}
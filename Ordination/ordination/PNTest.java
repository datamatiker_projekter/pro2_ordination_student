package ordination;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

import java.time.LocalDate;

import org.junit.Before;
import org.junit.Test;

public class PNTest
{
	private PN pn;
	private Laegemiddel lægemiddel;
	private Patient patient;

	@Before
	public void setUp() throws Exception
	{
		lægemiddel = new Laegemiddel("Acetylsalicylsyre", 0.1, 0.15, 0.16, "Styk");
		patient = new Patient("2222222-2222", "peter", 75);
		pn = new PN(LocalDate.of(2016, 8, 10), LocalDate.of(2016, 8, 20), lægemiddel, patient, -2);
	}

	@Test
	public void testSamletDosis()
	{
		PN pnTest = new PN(LocalDate.of(2016, 8, 10), LocalDate.of(2016, 8, 20), lægemiddel, patient, 2.0);
		for (int i = 0; i < 10; i++)
		{
			pnTest.givDosis(LocalDate.of(2016, 8, 11));
		}
		assertEquals(pnTest.samletDosis(), 20.0, 0.001);

		PN pnTest2 = new PN(LocalDate.of(2016, 8, 10), LocalDate.of(2016, 8, 20), lægemiddel, patient, 0);
		for (int i = 0; i < 10; i++)
		{
			pnTest2.givDosis(LocalDate.of(2016, 8, 11));
		}
		pnTest2.samletDosis();

	}

	@Test(expected = RuntimeException.class)
	public void testSamletdosis()
	{
		PN pnTest3 = new PN(LocalDate.of(2016, 8, 10), LocalDate.of(2016, 8, 20), lægemiddel, patient, -2);
		for (int i = 0; i < 10; i++)
		{
			pnTest3.givDosis(LocalDate.of(2016, 8, 11));
		}
		pnTest3.samletDosis();
	}

	// -------------------------------------------------------------------------------------------------------------------
	@Test
	public void testDoegnDosis()
	{
		PN pnTest = new PN(LocalDate.of(2016, 8, 10), LocalDate.of(2016, 8, 20), lægemiddel, patient, 20.0);
		pnTest.givDosis(LocalDate.of(2016, 8, 11));
		assertEquals(pnTest.doegnDosis(), 2.0, 0.01);

		PN pnTest2 = new PN(LocalDate.of(2016, 8, 10), LocalDate.of(2016, 8, 20), lægemiddel, patient, 0.0);
		pnTest.givDosis(LocalDate.of(2016, 8, 11));
		assertEquals(pnTest2.doegnDosis(), 0.0, 0.01);
	}

	@Test(expected = RuntimeException.class)
	public void testDoegndosis()
	{
		PN pnTest3 = new PN(LocalDate.of(2016, 8, 10), LocalDate.of(2016, 8, 20), lægemiddel, patient, -2);

		pnTest3.givDosis(LocalDate.of(2016, 8, 11));

		pnTest3.doegnDosis();
	}

	@Test
	public void testGetType()
	{
		assertEquals(pn.getType(), "PN");
	}

	@Test
	public void testGivDosis()
	{
		PN pnTest3 = new PN(LocalDate.of(2016, 8, 10), LocalDate.of(2016, 8, 20), lægemiddel, patient, -2);

		assertEquals(pnTest3.givDosis(LocalDate.of(2016, 8, 11)), true);
	}

	@Test
	public void testGetAntalGangeGivet()
	{
		fail("Not yet implemented");
	}

	@Test
	public void testGetAntalEnheder()
	{
		fail("Not yet implemented");
	}

}

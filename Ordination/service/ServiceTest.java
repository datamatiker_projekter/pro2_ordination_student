package service;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

import java.time.LocalDate;

import org.junit.Before;
import org.junit.Test;

import ordination.Laegemiddel;
import ordination.PN;
import ordination.Patient;

public class ServiceTest
{
	private PN pn;
	private Laegemiddel lægemiddel;
	private Patient patient;

	@Before
	public void setUp() throws Exception
	{

		lægemiddel = new Laegemiddel("Acetylsalicylsyre", 0.1, 0.15, 0.16, "Styk");
		patient = new Patient("222222-2222", "bob", 80);
		pn = new PN(LocalDate.of(2016, 8, 10), LocalDate.of(2016, 8, 24), lægemiddel, patient, 20);
	}

	@SuppressWarnings("deprecation")
	@Test
	public void testOpretPNOrdination()
	{
		PN pnTest = Service.getTestService().opretPNOrdination(LocalDate.of(2016, 8, 10), LocalDate.of(2016, 8, 24),
				patient, lægemiddel, 20);
		assertEquals(pnTest, pn);
	}

	@Test
	public void testOpretDagligFastOrdination()
	{
		fail("Not yet implemented");
	}

	@Test
	public void testOpretDagligSkaevOrdination()
	{
		fail("Not yet implemented");
	}

	@Test
	public void testOrdinationPNAnvendt()
	{
		fail("Not yet implemented");
	}

	@Test
	public void testAntalOrdinationerPrVægtPrLægemiddel()
	{
		fail("Not yet implemented");
	}

}
